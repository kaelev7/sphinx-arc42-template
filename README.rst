Using the Requirements documentation for PLC applications
=========================================================

before you can start you need to install two external dependencies:

* Python >= 3.10 https://www.python.org/downloads/windows/
* PlantUML https://plantuml.com/de/download

But the easiest way is to use a tool like chocolatey https://chocolatey.org/
Just install the PlantUML and Python package with it.


Now clone the project on your local machine and open it with VisualStudioCode.
Open a terminal window inside VSCode and continue with the following steps:


1. Create the python virtual environment
    ::

        python -m venv .venv

2. activate the virtual environment
    ::

        .\.venv\Scripts\Activate.ps1

3. Install the requirements
    ::

        pip install --upgrade pip

        pip install -r requirements.txt

4. Run and preview the documentation
    ::

        sphinx-autobuild doc build/html