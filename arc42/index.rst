.. Sphinx Arc42 Template documentation master file, created by
   sphinx-quickstart on Sun Oct 30 12:58:32 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx Arc42 Template!
=================================

**About arc42**

arc42, the template for documentation of software and system
architecture.

Template Version 8 EN. (based upon AsciiDoc version), May 2022

Created, maintained and © by Dr. Peter Hruschka, Dr. Gernot Starke and
contributors. See https://arc42.org.

.. note::

   This version of the template contains some help and explanations. It
   is used for familiarization with arc42 and the understanding of the
   concepts. For documentation of your own system you use better the
   *plain* version.

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Arc42 Template:

   01_IntroductionAndGoals.rst
   02_ArchitectureConstraints.rst
   03_SystemScopeAndContext.rst
   04_SolutionStrategy.rst
   05_BuildingBlockView.rst
   06_RuntimeView.rst
   07_DeploymentView.rst
   08_CrossCuttingConcepts.rst
   09_ArchitectureDecisions.rst
   10_QualityRequirements.rst
   11_RisksAndTechnicalDebts.rst
   12_Glossary.rst

.. toctree::
   :maxdepth: 2
   :Caption: About 

   About/ThisTemplate.rst
   About/arc42.rst




