.. _section-glossary:

Glossary
========

.. details:: **Contents**
    :class: info
    :open:   

    The most important domain and technical terms that your stakeholders use
    when discussing the system.

    You can also see the glossary as source for translations if you work in
    multi-language teams.

.. details:: **Motivation**
    :class: tldr
    :open:

    You should clearly define your terms, so that all stakeholders

    *  have an identical understanding of these terms

    *  do not use synonyms and homonyms

    *  A table with columns <Term> and <Definition>.

    *  Potentially more columns in case you need translations.

..  details:: Form
    :class: example
    :open:


See `Glossary <https://docs.arc42.org/section-12/>`__ in the arc42
documentation.

.. glossary::

    arc42
        arc42, the Template for documentation of software and system architecture.

    stakeholders
        a person with an interest or concern in the product or the documentation.



.. |arc42| image:: _static/arc42-logo.png