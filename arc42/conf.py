# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Sphinx Arc42 Template'
copyright = '2022, Kaelev7'
author = 'Kaelev7'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinxcontrib.plantuml",
    "sphinx.ext.autodoc",
    "matplotlib.sphinxext.plot_directive",
    "sphinx_copybutton",
    "sphinxcontrib.programoutput",
    "sphinx_design",
    "sphinx.ext.duration",
    "sphinx_immaterial",
    "sphinx_immaterial.graphviz",
    "sphinxcontrib.details.directive",
]

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_immaterial'
html_static_path = ['_static']

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.

html_sidebars = {
    "**": ["about.html", "navigation.html", "searchbox.html"],
}

html_logo = "./_static/arc42-logo.png"
# html_favicon = "./_static/sphinx-needs-logo-favicon.png"
# material theme options (see theme.conf for more information)
# http://172.16.240.10/Software/BECKHOFF/dokumentation/CodingGuidelines/-/tree/main/02_NamingRules/index.rst
html_theme_options = {
    "icon": {
        "repo": "fontawesome/brands/gitlab",
    },
    "site_url": "https://sphinxcontrib-needs.readthedocs.io/",
    "repo_url": "https://gitlab.com/kaelev7/sphinx-arc42-template",
    "repo_name": "arc42 template for sphinx",
    "repo_type": "gitlab",
    "edit_uri": "-/blob/main//doc",
    # "google_analytics": ["UA-XXXXX", "auto"],
    "globaltoc_collapse": False,
    "features": [
        "navigation.sections",
        "navigation.top",
        "search.share",
    ],
    "palette": [
        {
            "media": "(prefers-color-scheme: light)",
            "scheme": "default",
            "primary": "indigo",
            "accent": "light-blue",
            "toggle": {
                "icon": "material/weather-night",
                "name": "Switch to dark mode",
            },
        },
        {
            "media": "(prefers-color-scheme: dark)",
            "scheme": "slate",
            "primary": "indigo",
            "accent": "light-blue",
            "toggle": {
                "icon": "material/weather-sunny",
                "name": "Switch to light mode",
            },
        },
    ],
    "toc_title_is_page_title": True,
}


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
html_css_files = ["custom.css"]
