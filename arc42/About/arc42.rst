About arc42
===========

This information should stay in every repository as per their license: https://arc42.org/license

arc42, the Template for documentation of software and system architecture.

By Dr. Gernot Starke, Dr. Peter Hruschka and contributors.

Template Revision: 8 EN (based on asciidoc), released February 2022.

© We acknowledge that this document uses material from the arc 42
architecture template, http://www.arc42.org. Created by Dr. Peter
Hruschka & Dr. Gernot Starke. For additional contributors see
https://github.com/arc42/arc42-template/graphs/contributors

.. note::

    This version of the template contains some help and explanations. It
    is used for familiarization with arc42 and the understanding of the
    concepts. For documentation of your own system you use better the
    *plain* version.


Literature and references
--------------------------

Starke-2014
    Gernot Starke: Effektive Softwarearchitekturen - Ein praktischer
    Leitfaden. Carl Hanser Verlag, 6, Auflage 2014.
Starke-Hruschka-2011
    Gernot Starke und Peter Hruschka: Softwarearchitektur kompakt.
    Springer Akademischer Verlag, 2. Auflage 2011.
Zörner-2013
    Softwarearchitekturen dokumentieren und kommunizieren, Carl Hanser
    Verlag, 2012

Online Examples
---------------

Here you find links to public arc42 examples. Contact us if you like your system included here…

-  `HTML Sanity Checker <https://hsc.aim42.org/>`__  (English)

   Verbose example for the documentation of a Gradle plugin, created by Dr. Gernot Starke.

-  `DocChess <https://www.dokchess.de//>`__ (German)

   Verbose example for a chess engine, created by Stefan Zörner. There is also a book available which describes the creation of this example.

-  `Gradle <https://www.embarc.de/arc42-starschnitt-gradle/>`__ (German)

   A series of blog posts which describe certain aspects of Gradle and put them into context of arc42. Created by Stefan Zörner.

-  `biking <https://biking.michael-simons.eu/docs/index.html>`__ (English)

   A real world example for a bike activity tracker, created by Michael Simons


Acknowledgements and collaborations
----------------------------------------------------

arc42 originally envisioned by `Dr. Peter
Hruschka <http://b-agile.de>`__ and `Dr. Gernot
Starke <http://gernotstarke.de>`__.

Sources
    We maintain arc42 in *asciidoc* format at the moment, hosted in
    `GitHub under the arc42 project repository <https://github.com/arc42/arc42-template>`__.
Issues
    We maintain a list of `open topics and
    bugs <https://github.com/arc42/arc42-template/issues>`__.

We are looking forward to your corrections and clarifications! Please
fork the repository mentioned over this lines and send us a *pull
request*!

